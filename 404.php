<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<section id="error-404 not-found">
		<div class="container">
			<header class="page-header">
				<h1 class="page-title">Deze pagina kan niet worden gevonden</h1>
			</header>

			<div class="page-content">
				<p>De pagina die u heeft opgevraagd is verwijderd of aangepast. Ga terug naar de <a href="<?php home_url(); ?>">homepage</a>.</p>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
