<?php get_header(); ?>
	<section id="pageheader">
		<div class="container">
			<h1><?php bloginfo( 'name' ); ?></h1>
			<?php
			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p><?php echo $description; ?></p>
			<?php endif; ?>
		</div>
	</section>

	<?php if ( is_active_sidebar( 'home' ) ) : ?>
	<section id="homepage">
		<?php dynamic_sidebar( 'home' ); ?>
	</section>
	<?php endif; ?>
<?php get_footer(); ?>
