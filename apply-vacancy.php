<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

//include jquery in header for inline script
wp_enqueue_script( 'jquery' );

get_header(); ?>

	<article class="vacature">

		<div class="container">
			<section id="content-header">
				<h1><?php the_title(); ?></h1>
			</section>
			<section id="content">
				<div class="row">
					<div class="col-md-8">
						<form method="POST" id="applyForm" action="<?php the_applicationlink(); ?>" enctype="multipart/form-data">
							<?php the_application_errors(); ?>

							<p class="comment-notes">
								 Verplichte velden zijn gemarkeerd met <span class="required">*</span>
							</p>
							<div class="row">
								<div class="form-group col-md-12">
									<?php the_email_field('E-mailadres', true); ?>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<?php the_firstname_field('Voornaam', false); ?>
								</div>
								<div class="form-group col-md-6">
									<?php the_lastname_field(); ?>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<?php the_telephone_field(); ?>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="cv">Upload je CV <span class="required">*</span> <span class="form-allowed-tags">doc, docx, pdf, txt, odt of rtf. Max: 8 MB.</span></label>

									<ul class="list-input-file">
										<li class="input-file">
											<div class="file-info">
												<span class="file-name">&nbsp;&nbsp;filename</span>
												<button class="file-remove fa fa-times"></button>
											</div>
											<div class="btn btn-upload btn-secondary-hover">
												<span><i class="fa fa-plus"></i>&nbsp;&nbsp;cv uploaden</span>
												<?php the_cv_field(null); ?>
											</div>
										</li>
									</ul>
									<p id="cvErrorContainer">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="cv">Motivatie <span class="form-allowed-tags">doc, docx, pdf, txt, odt of rtf. Max: 8 MB.</span></label>

									<ul class="list-input-file">
										<li class="input-file">
											<div class="file-info">
												<span class="file-name">&nbsp;&nbsp;filename</span>
												<button class="file-remove fa fa-times"></button>
											</div>
											<div class="btn btn-upload btn-secondary-hover">
												<span><i class="fa fa-plus"></i>&nbsp;&nbsp;motivatie uploaden</span>
												<?php the_motivationfile_field(null); ?>
											</div>
										</li>
									</ul>
									<p id="motivationErrorContainer">
								</div>
							</div>
							<p class="comment-form-comment">
								<?php the_motivation_field(null); ?>
							</p>

							<p class="form-submit">
                                <input type="submit" class="submit btn btn-primary btn-lg" value="Sollicitatie versturen">
							</p>
						</form>
					</div>
					<div class="col-md-4 widgets-right">
						<?php if ( is_active_sidebar( 'applyform_sidebar' ) ) : ?>
						<section id="vacancy-sidebar">
							<?php dynamic_sidebar( 'applyform_sidebar' ); ?>
						</section>
						<?php endif; ?>
					</div>
				</div>

			</section>

            <?php wp_enqueue_script( 'apply' ); ?>

		</div>
	</article>

<?php get_footer(); ?>

<script src="<?= get_template_directory_uri(); ?>/assets/js/library/parsley.min.js"></script>
<script src="<?= get_template_directory_uri(); ?>/assets/js/library/parsley.nl.js"></script>
<script>

    window.Parsley.setLocale('nl');

    window.Parsley.addValidator('fileDocument', {
        requirementType: 'string',
        validateString: function(value, requirement) {
            value = value.toLowerCase();
            return value.indexOf('.doc') >= 0 || value.indexOf('.docx') >= 0 || value.indexOf('.pdf') >= 0 || value.indexOf('.odt') >= 0;
        },
        messages: {
            en: 'This is not a valid document',
            nl: 'Dit bestandstype is niet toegestaan'
        }
    });


    jQuery(function() {
        var email = jQuery('input#email').parsley({
            type: 'email'
        });
        var documentCV = jQuery('input#cv').parsley({
            fileDocument: true,
            validateIfEmpty: true,
            errorsContainer: '#cvErrorContainer'
        });

        jQuery('#applyForm').parsley({
            trigger: 'change'
        })
            .on('field:validated', function() {
                var ok = jQuery('.parsley-error').length === 0;
                jQuery('.bs-callout-info').toggleClass('hidden', !ok);
                jQuery('.bs-callout-warning').toggleClass('hidden', ok);
            })
            .on('form:submit', function() {
                console.log('disable that button!');
                jQuery('#applyForm #submit').prop('disabled', true);
                return true;
            });

        jQuery(document).ready(function($){
            $(".list-input-file").customFileInput();
        });
    });
</script>
