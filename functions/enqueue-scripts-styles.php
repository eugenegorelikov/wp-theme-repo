<?php
/**
 * Manage scripts and styles
 */
class enqueue_scripts_styles
{
    protected $manifest;

    public function __construct()
    {
        $this->manifest = $this->get_manifest();

        add_action( 'wp_enqueue_scripts', [ $this, 'styles' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ] );
        add_action( 'wp_footer',          [ $this, 'dequeue' ] );
    }

    protected function get_manifest()
    {
        $manifest = file_get_contents(
            get_template_directory() .
            '/dist/mix-manifest.json' );

        $manifest = json_decode( $manifest );

        return $manifest;
    }

    protected function mix( $path )
    {
        return get_template_directory_uri() . '/dist'
             . $this->manifest->$path;
    }

    public function styles()
    {
        wp_enqueue_style(
            'app',
            $this->mix( '/css/app.css' ),
            [],
            null
        );
    }

    public function scripts()
    {
        // Couldn't include bootstrap in build
        wp_enqueue_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', ['jquery'], '3.3.6', true);

        wp_enqueue_script('ofi', get_template_directory_uri().'/assets/js/ofi.browser.js', [], false, true);

        // The main app.js with localized vars
        wp_register_script(
            'app',
            $this->mix( '/js/app.js' ),
            ['jquery', 'bootstrap', 'ofi'],
            null,
            true
        );
        wp_localize_script('app', 'ajaxurl', admin_url('admin-ajax.php'));
        wp_enqueue_script('app');

         //HTML5.js for IE9
        wp_enqueue_script(
            'html5',
            get_template_directory_uri().'/assets/js/library/html5.js'
        );
        wp_script_add_data(
            'html5',
            'conditional',
            'lt IE 9'
        );
    }

    public function dequeue()
    {
        wp_dequeue_script('wp-embed');
    }
}

new enqueue_scripts_styles;
