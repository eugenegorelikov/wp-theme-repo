<?php

add_action( 'wp_ajax_share_vacancy_email', 'share_vacancy_email' );
add_action( 'wp_ajax_nopriv_share_vacancy_email', 'share_vacancy_email' );

function share_vacancy_email()
{
    $email_sender = $_POST['email_sender'];
    $email_receiver = $_POST['email_receiver'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    $success = wp_mail($email_receiver, $subject, $content);

    if ($success) {
        wp_send_json_success(['message' => 'Versturen van de email is gelukt.']);
    }

    wp_send_json_error(['message' => 'Versturen van de email is mislukt.']);
}
