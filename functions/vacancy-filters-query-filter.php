<?php
/**
 * Set query vars for selected filter groups.
 */
add_filter( 'query_vars', function( $vars )
{

    if (function_exists('have_rows')) {
        while( have_rows( 'active_filters', 'option' ) ) {
            the_row();
            $vars[] = get_row_layout();
        }
    }

    $vars[] = 'keywords';

    return $vars;
});

/**
 * Handle query vars, translate to WordPress' Query.
 */
add_action('pre_get_posts', function( $query )
{
    if ( $query->is_archive()
        && $query->is_main_query()
        && $query->query['post_type'] == 'vacancy'
        && '' !== $_SERVER['QUERY_STRING']
    ) {
        $query_string   = '?' . $_SERVER['QUERY_STRING'];
        $too_api = new Vacancy_Filters( $query_string );

        /**
         * Insert fake data to get 0 results instead of all results from WP
         */
        if(empty($too_api->job_ids)) {
            $too_api->job_ids[] = 'fake_data_to_get_0_results';
        }

        $meta_query = [[
            'key' => 'tooID',
            'value' => $too_api->job_ids,
            'compare' => 'IN'
        ]];

        $query->set('meta_query', $meta_query);


    }
});
