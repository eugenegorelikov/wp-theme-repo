<?php
/**
 * Vacancy filters
 */
class Vacancy_Filters
{
    public $jobs = [];
    public $job_ids = [];
    public $jobs_found = [];

    public $filters = [];
    public $filters_selected = [];

    private $show_search;
    private $channel_token;

    /**
     * Property name mapping
     *
     * @var array
     */
    private $property_name_mapping = [
        'fields' => 'discipline'
    ];

    public function __construct( $query = '' )
    {
        $this->show_search   = get_field('show_search', 'option');
        $this->channel_token = get_field('too_channel_token', 'option');

        $this->jobs    = $this->get_api_data();
        $this->filters = $this->get_filters();

        if ( '' != $query ) {
            $this->jobs_found       = $this->get_api_data( $query );
            $this->filters_selected = $this->get_selected( $query );
        }
    }

    protected function get_selected( $query )
    {
        $query = str_replace('?', '', $query);
        parse_str( $query, $query_parts );

        $selected = [];

        foreach( $query_parts as $key => $filter ) {
            $filter = str_replace('[', '', $filter);
            $filter = str_replace(']', '', $filter);
            $filter = explode(',', $filter);

            $selected[$key] = $filter;
        }

        return $selected;
    }

    public function render()
    {
        $html = '<form id="filter-bar" action="">';

        if ( $this->show_search ) {
            $html .= $this->render_search();
        }

        if ( have_rows('active_filters', 'option') ) {
            $html .= '<div class="background-light-gray"><div class="container"><a href="#section-filters-main-wrapper" class="collapse-toggle collapse-toggle-sm collapsed" data-toggle="collapse" aria-expanded="false">Filter resultaten</a>
            <div class="filter-tags"></div>
            <div id="section-filters-main-wrapper" aria-expanded="false" class="collapse">
                <ul class="panel-group list-filter-categories" id="list-filter-categories">';
        }

        while( have_rows( 'active_filters', 'option' ) ) {
            the_row();

            $property = get_row_layout();

            $html .= $this->render_filter( $property );

        }

        if ( have_rows('active_filters', 'option') ) {
            $html .= '</ul></div></div></div>';
        }

        $html .= '</form>';

        return $html;
    }

    protected function get_api_data( $query = '' )
    {
        $results = wp_remote_get(
            'http://www.jobsrepublictoo.nl/public-api/v1/vacancy' . $query,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-Channel' => $this->channel_token
                ]
            ]
        );

        if (is_wp_error($results))
        {
            return;
        }

        $results = json_decode( $results['body'], true );

        if ( '' !== $query ) {
            $ids = [];

            if ( ! empty( $results ) ) {
                foreach( $results as $job ) {
                    $ids[] = $job['id'];
                }
            }

            $this->job_ids = $ids;
        }

        return $results;
    }

    protected function render_search()
    {
        return '
        <div class="background-light-blue">
            <div class="container">
                <input name="keywords" type="text" class="form-control" placeholder="Functie, trefwoord of bedrijfsnaam">
                <input class="btn btn-primary" type="submit" value="Zoeken">
            </div>
        </div>
        ';
    }

    protected function render_filter( $property = false )
    {
        if ( ! $property )
            return '';

        /**
         * Skip the LI output if no options are available
         */
        if (empty($this->filters[$property]['options'])) {
            return '';
        }

        $html = '
        <li class="panel" data-option="' . $property . '">
            <a href="#filters-' . $property . '" class="collapse-toggle-submenu collapsed" data-toggle="collapse" data-parent="#list-filter-categories" aria-expanded="false">' . $this->filters[$property]['label'] . '</a>
            <div class="section-filter-options collapse" id="filters-' . $property . '" data-component="FixedButton" data-initialized="true" style="">
                <ul class="list-filter-options">
        ';

        foreach( $this->filters[$property]['options'] as $key => $value ) {

            $id   = $property . '_' . $key;
            $name = $property;
            $checked = '';

            /**
             * Map the name property if it is available
             */
            if (key_exists($name, $this->property_name_mapping)) {
                $name = $this->property_name_mapping[$name];
            }

            if (isset($_GET[$name]) && isset($value)) {
                if(strrpos( $_GET[$name], $value)){
                    $checked="checked='checked'";
                }
            }

            $html .= '
            <li>
                <div class="custom-check-radio">
                    <label for="'. $id .'">
                    <input type="checkbox" id="'. $id .'" name="'. $name .'" value="'. $value .'" data-option-value="'. $value .'" ' . $checked . '>
                    '. $value .'</label>
                </div>
            </li>
            ';
        }

        $html .= '</ul></div></li>';

        return $html;
    }

    protected function recursive_array_search( $needle, $haystack )
    {
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    protected function get_filters()
    {
        $layout = get_field_object('active_filters', 'option');
        $filters = [];

        while( have_rows( 'active_filters', 'option' ) ) {
            the_row();

            $key = get_row_layout(); // fields, categories
            $acf_key = $this->recursive_array_search($key,$layout['layouts']);
            $label = $layout['layouts'][ $acf_key ]['label'];

            $options = [];

            foreach( $this->jobs as $job ) {
                if ( isset( $job[ $key ] ) )
                    $options = array_merge( $options, $job[ $key ] );

                if ( $key == 'region' )
                    $options = array_merge( $options, [ $job['location']['region'] ] );
            }

            /**
             * Use SORT_REGULAR flag to prevent array_unique Array to string conversion notice
             */
            $options = array_filter( array_unique( $options, SORT_REGULAR ) );
            asort( $options );

            if ( $key == 'region' )
                $options = get_full_region_names($options, true);

            $filters[$key] = [
                'label' => $label,
                'options' => $options,
            ];
        }

        return $filters;
    }
}

function get_full_region_names($regions, $return_mapping = false)
{
    $region_mapping = [
        'DR' => 'Drenthe',
        'FL' => 'Flevoland',
        'FR' => 'Friesland',
        'GE' => 'Gelderland',
        'GR' => 'Groningen',
        'LI' => 'Limburg',
        'NB' => 'Noord-Brabant',
        'NH' => 'Noord-Holland',
        'OV' => 'Overijssel',
        'UT' => 'Utrecht',
        'ZE' => 'Zeeland',
        'ZH' => 'Zuid-Holland',
    ];

    /**
     * Show all regions
     */
    if ($return_mapping) {
        return $region_mapping;
    }

    foreach ($regions as $index => $region) {
        $regions[$index] = $region_mapping[$region];
    }

    return $regions;
}

function the_search_filters( $vue = false ) {
    if ( $vue ) {
        echo '<filters></filters>';
    } else {
        $search_filters = new Vacancy_Filters;
        echo $search_filters->render();
    }
}
