<?php

add_filter( 'acf/save_post', function( $post_id )
{
    if ( empty( $_POST['acf'] ) ) {
        return;
    }

    clean_post_cache( $post_id );

    $acf_cache_cleared = wp_cache_delete( 'acf-post', 'acf' );

    if ( !$acf_cache_cleared ) {
        wp_cache_flush();
    }
});
