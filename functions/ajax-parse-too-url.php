<?php

add_action( 'wp_ajax_too_filter', 'too_filter' );
add_action( 'wp_ajax_nopriv_too_filter', 'too_filter' );

function too_filter()
{
    $data  = $_REQUEST['form'];
    $parts = [];

    foreach( $data as $value ) {
        if ( $value['name'] == 'keywords' ) {
            if ( '' !== $value['value'] )
                $parts[$value['name']] = $value['value'];
        } else {
            $parts[$value['name']][] = $value['value'];
        }
    }

    foreach( $parts as $key => $value ) {
        if ( is_array( $value ) ) {
            $parts[$key] = '['.implode(',', $value).']';
        }
    }

    $query = '?' . http_build_query( $parts );
    $query = str_replace('%5B', '[', $query);
    $query = str_replace('%2C', ',', $query);
    $query = str_replace('%5D', ']', $query);

    wp_send_json_success( $query );
}
