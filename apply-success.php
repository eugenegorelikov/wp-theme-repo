<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

//include jquery in header for inline script
wp_enqueue_script( 'jquery' );

get_header(); ?>
	<div class="container">
		<a href="<?php echo get_home_url(); ?>" class="gotohome"><i class="fa fa-angle-left"></i>home</a>
		<a href="<?php the_permalink(); ?>" class="gotohome"><i class="fa fa-angle-left"></i>vacature</a>
		<a href="<?php the_applicationlink(); ?>" class="gotohome"><i class="fa fa-angle-left"></i>vacature</a>
	</div>
	
	<article class="vacature">
	
		<div class="container">
			<section id="content-header">
				<div class="container">
					<h1><?php the_title(); ?></h1>
				</div>
			</section>
			<section id="content">
				<div class="container">
					
					<p>Bedankt voor het solliciteren op de vacature.</p>
					<p>We nemen zo spoedig mogelijk contact met u op.</p>
					
				</div>
			</section>
		
		</div>
	</article>

<?php get_footer(); ?>