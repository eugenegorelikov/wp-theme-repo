<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />		
		<script>(function(){document.documentElement.className='js'})();</script>
		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">

                    <?php if (wp_nav_menu(['menu' => 'primary', 'theme_location' => 'primary', 'echo' => false, 'fallback_cb' => false ])) : ?>

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#one-primary-menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    <?php endif; ?>

					<?php if ( get_theme_mod( 'jobsrepublic_one_logo' ) ) : ?>
						<a href="<?php echo get_home_url(); ?>" class="logo">
							<img src="<?php echo get_theme_mod( 'jobsrepublic_one_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
					<?php else: ?>
						<a href="<?php echo get_home_url(); ?>">
							<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
						</a>
					<?php endif; ?>
				</div>

				<div class="collapse navbar-collapse" id="one-primary-menu">

				<?php if ( has_nav_menu( 'primary' ) ): ?>

					<?php
                        wp_nav_menu([
                            'menu'              => 'primary',
                            'theme_location'    => 'primary',
                            'depth'             => 2,
                            'container'         => 'ul',
                            'fallback_cb'       => false,
                            'container_id'      => 'one-primary-menu',
                            'menu_class'        => 'nav navbar-nav navbar-right',
                            'walker'            => new wp_bootstrap_navwalker()
                        ]);
                    ?>

				<?php elseif ( is_active_sidebar( 'header_right' )) : ?>

					<div class="nav navbar-nav navbar-right">
						<?php dynamic_sidebar( 'header_right' ); ?>
					</div>

				<?php endif; ?>

				</div>
			</div>
		</nav>

		<main>
