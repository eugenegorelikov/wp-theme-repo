/**
 * We'll load jQuery which provides support for JavaScript based
 * features such as modals and tabs. This code may be modified
 * to fit the specific needs of your application.
 */
window.$ = window.jQuery = require('jquery');

/**
 * Attach the Vue to the window.
 */
window.Vue = require('vue');

/**
 * Axios, for ajax requests and handling.
 */
window.axios = require('axios');
