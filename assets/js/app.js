/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./library/parsley.min.js');
require('./library/parsley.nl.js');
require('./library/customfileinput.js');
require('./library/sticky-kit.min.js');

var JobAlerts = require('./library/JobAlerts.js');
import Scroller from './library/Scroller';

var CookieMessage = require('./library/CookieMessage.js');

(function (document, window, $) {

  $(document).ready(function () {

    addFlexibleContainer($);

    window.addEventListener('resize', function(event) {

      addFlexibleContainer($);

    });

    new objectFitImages();

    new CookieMessage().init();

    createAlerts();

    createFilterBar();

    var test = new Scroller();
    test.addTestElements('js-scroll')
      .windowScrollHandler();

    window.scroller = test;
  });


})(document, window, jQuery);

function addFlexibleContainer($) {
  "use strict";

  let iframe = $('iframe'),
    wrapper = iframe.parent().hasClass('iframe-flexible-container');

  if (window.matchMedia("(max-width: 1023px)").matches) {

    if (wrapper) {
      return;
    }
    iframe.wrap("<div class='iframe-flexible-container'></div>");

  } else {

    if (!wrapper) {
      return;
    }
    iframe.unwrap("<div class='iframe-flexible-container'></div>");

  }


}

function createAlerts() {

  var jobAlertForm = document.getElementsByClassName('widget_jobalerts');

  if (jobAlertForm) {

    for (var index = 0; index < jobAlertForm.length; index++) {

      var jobalerts = new JobAlerts(ajaxurl, jobAlertForm[index]);
      jobalerts.init();

    }

  }

}

function createFilterBar() {

  var currentUrl = window.location.href.split('?')[0];

  jQuery('.custom-check-radio input').on('click', function () {

    var formData = jQuery('#filter-bar').serializeArray();

    jQuery.ajax({
      type: 'POST',
      url: ajaxurl,
      data: {
        "action": "too_filter",
        "form": formData
      },
      dataType: 'json'
    }).success(function (response) {
      if (response.success) {
        window.location.href = currentUrl + response.data;
      }
    });
  });
}
