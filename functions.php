<?php
require_once('functions/allow-jobs-iframes.php');
require_once('functions/wp-remove-emoji.php');
require_once 'functions/wp-bootstrap-navwalker.php';

require_once('functions/enqueue-scripts-styles.php');

require_once('functions/action-widgets-init.php');
require_once('functions/action-after-setup-theme.php');
require_once('functions/action-customize-register.php');

require_once('functions/acf-save-json.php');
require_once('functions/acf-options-page.php');
require_once('functions/acf-flush-cache.php');

require_once('functions/too-plugin-check.php');
require_once('functions/too-migrate-to-acf.php');

require_once('functions/vacancy-properties.php');
require_once('functions/vacancy-filters.php');
require_once('functions/vacancy-filters-query-filter.php');
require_once('functions/vacancy-forms.php');

require_once('functions/ajax-too-job-alert.php');
require_once('functions/ajax-vacancy-share-email.php');
require_once('functions/ajax-parse-too-url.php');
